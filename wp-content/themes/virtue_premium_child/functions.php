<?php
// define global file locations 
define( 'HOME_URI', home_url() );
define( 'THEME_URI', get_stylesheet_directory_uri() );
define( 'IMG', THEME_URI . '/img/' );
define( 'THEME_JS', THEME_URI . '/js' );

// Update CSS within in Admin
function admin_style() {
  wp_enqueue_style('admin-styles', THEME_URI.'/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');


// Register Custom Post Type
// function cpt_dessert() {

// 	$labels = array(
// 		'name'                => _x( 'Desserts', 'Post Type General Name', 'text_domain' ),
// 		'singular_name'       => _x( 'Dessert', 'Post Type Singular Name', 'text_domain' ),
// 		'menu_name'           => __( 'Dessert', 'text_domain' ),
// 		'parent_item_colon'   => __( 'Parent Dessert:', 'text_domain' ),
// 		'all_items'           => __( 'All Desserts', 'text_domain' ),
// 		'view_item'           => __( 'View Dessert', 'text_domain' ),
// 		'add_new_item'        => __( 'Add New Dessert', 'text_domain' ),
// 		'add_new'             => __( 'New Dessert', 'text_domain' ),
// 		'edit_item'           => __( 'Edit Dessert', 'text_domain' ),
// 		'update_item'         => __( 'Update Dessert', 'text_domain' ),
// 		'search_items'        => __( 'Search Desserts', 'text_domain' ),
// 		'not_found'           => __( 'No desserts found', 'text_domain' ),
// 		'not_found_in_trash'  => __( 'No desserts found in Trash', 'text_domain' ),
// 	);
// 	$args = array(
// 		'label'               => __( 'dessert', 'text_domain' ),
// 		'description'         => __( 'Dessert information pages', 'text_domain' ),
// 		'labels'              => $labels,
// 		'supports'            => array( 'title', 'editor' ),
// 		'taxonomies'          => array( 'category', ' flavours', 'dietary' ),
// 		'hierarchical'        => false,
// 		'public'              => true,
// 		'show_ui'             => true,
// 		'show_in_menu'        => true,
// 		'show_in_nav_menus'   => true,
// 		'show_in_admin_bar'   => true,
// 		'menu_position'       => 20,
// 		'menu_icon'           => '/wp-content/uploads/2013/09/dessert-icon.png',
// 		'can_export'          => true,
// 		'has_archive'         => true,
// 		'exclude_from_search' => false,
// 		'publicly_queryable'  => true,
// 		'capability_type'     => 'page',
// 	);
// 	register_post_type( 'dessert', $args );

// }

// // Hook into the 'init' action
// add_action( 'init', 'cpt_dessert', 0 );




// Register Custom Post Types
function cpt_desserts() {

	$labels = array(
		'name'                => _x( 'Desserts', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Dessert', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Desserts', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Dessert:', 'text_domain' ),
		'all_items'           => __( 'All Desserts', 'text_domain' ),
		'view_item'           => __( 'View Dessert', 'text_domain' ),
		'add_new_item'        => __( 'Add New Dessert', 'text_domain' ),
		'add_new'             => __( 'New Dessert', 'text_domain' ),
		'edit_item'           => __( 'Edit Dessert', 'text_domain' ),
		'update_item'         => __( 'Update Dessert', 'text_domain' ),
		'search_items'        => __( 'Search Desserts', 'text_domain' ),
		'not_found'           => __( 'No desserts found', 'text_domain' ),
		'not_found_in_trash'  => __( 'No desserts found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'desserts', 'text_domain' ),
		'description'         => __( 'Dessert information pages', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor' ),
		'taxonomies'          => array( 'dessert_types', 'flavours', 'dietary' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 20,
		'menu_icon'           => IMG.'dessert-icon.png',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'desserts', $args );

}
add_action( 'init', 'cpt_desserts', 0 );




// Register Custom Taxonomy
function dessert_type_taxonomy()  {

	$labels = array(
		'name'                       => _x( 'Dessert Types', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Dessert Type', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Dessert Types', 'text_domain' ),
		'all_items'                  => __( 'All Dessert Types', 'text_domain' ),
		'parent_item'                => __( 'Parent Dessert Type', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Dessert Type:', 'text_domain' ),
		'new_item_name'              => __( 'New Dessert Type', 'text_domain' ),
		'add_new_item'               => __( 'Add New Dessert Type', 'text_domain' ),
		'edit_item'                  => __( 'Edit Dessert Type', 'text_domain' ),
		'update_item'                => __( 'Update Dessert Type', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate Dessert Types with commas', 'text_domain' ),
		'search_items'               => __( 'Search Dessert Types', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove Dessert Type', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Dessert Type', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'dessert-types', 'desserts', $args );

}
add_action( 'init', 'dessert_type_taxonomy', 0 );


function flavour_taxonomy()  {

	$labels = array(
		'name'                       => _x( 'Flavours', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Flavour', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Flavours', 'text_domain' ),
		'all_items'                  => __( 'All Flavours', 'text_domain' ),
		'parent_item'                => __( 'Parent Flavour', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Flavour:', 'text_domain' ),
		'new_item_name'              => __( 'New FlavourName', 'text_domain' ),
		'add_new_item'               => __( 'Add New Flavour', 'text_domain' ),
		'edit_item'                  => __( 'Edit Flavour', 'text_domain' ),
		'update_item'                => __( 'Update Flavour', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate Flavours with commas', 'text_domain' ),
		'search_items'               => __( 'Search Flavour', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove Flavour', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used Flavour', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'flavours', 'desserts', $args );

}
add_action( 'init', 'flavour_taxonomy', 0 );

function dietary_taxonomy()  {

	$labels = array(
		'name'                       => _x( 'Dietary', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Dietary', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Dietary', 'text_domain' ),
		'all_items'                  => __( 'All dietary', 'text_domain' ),
		'parent_item'                => __( 'Parent dietary', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent dietary:', 'text_domain' ),
		'new_item_name'              => __( 'New Dietary Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Dietary', 'text_domain' ),
		'edit_item'                  => __( 'Edit dietary', 'text_domain' ),
		'update_item'                => __( 'Update dietary', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate dietary with commas', 'text_domain' ),
		'search_items'               => __( 'Search dietary', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove dietary', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used dietary', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => false,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'dietary', 'desserts', $args );

}
add_action( 'init', 'dietary_taxonomy', 0 );



// add_action( 'init', 'unregister_taxonomy1');
// function unregister_taxonomy1(){
// 	global $wp_taxonomies;
// 	$taxonomy = 'dietary';
// 	if ( taxonomy_exists( $taxonomy)) {
// 		unset( $wp_taxonomies[$taxonomy]);
// 	}

// }



// add_action( 'init', 'unregister_taxonomy2');
// function unregister_taxonomy2(){
// 	global $wp_taxonomies;
// 	$taxonomy = 'flavours';
// 	if ( taxonomy_exists( $taxonomy)) {
// 		unset( $wp_taxonomies[$taxonomy]);
// 	}

// }

// add_action( 'init', 'unregister_taxonomy3');
// function unregister_taxonomy3(){
// 	global $wp_taxonomies;
// 	$taxonomy = 'category';
// 	if ( taxonomy_exists( $taxonomy)) {
// 		unset( $wp_taxonomies[$taxonomy]);
// 	}

// }


//DEBUG SHOW TEMPLATE

// add_filter( 'template_include', 'var_template_include', 1000 );
// function var_template_include( $t ){
//      $GLOBALS['current_theme_template'] = basename($t);
//      return $t;
// }

// function get_current_template( $echo = false ) {
//      if( !isset( $GLOBALS['current_theme_template'] ) )
//          return false;
//      if( $echo )
//          echo $GLOBALS['current_theme_template'];
//      else
//          return $GLOBALS['current_theme_template'];
// }

?>