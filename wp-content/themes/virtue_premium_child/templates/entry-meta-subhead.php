<?php // modified this template to use 'get terms' instead of categories so we can deal with dessert types properly - gareth (larkscapes) ?>

<div class="subhead">
    <?php if ($post->post_type == 'post') { // original code with conditional for 'post' post type - so blog still works?>
        <span class="postauthortop author vcard">
            <i class="icon-user2"></i> <?php global $virtue_premium; if(!empty($virtue_premium['post_by_text'])) {$authorbytext = $virtue_premium['post_by_text'];} else {$authorbytext = __('by', 'Rizo');} echo $authorbytext; ?>  <span itemprop="author"><a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="fn" rel="author"><?php echo get_the_author() ?></a></span> |
        </span>
        <?php $post_category = get_the_category(); if ( $post_category==true ) { 
        if(!empty($virtue_premium['post_incat_text'])) {$incattext = $virtue_premium['post_incat_text'];} else {$incattext = __('posted in:', 'virtue');}
        ?>  <span class="postedintop"><i class="icon-drawer"></i> <?php echo $incattext;?> <?php the_category(', ') ?> </span>
        <span class="kad-hidepostedin">|</span><?php }?>
        <span class="postcommentscount">
        <a href="<?php the_permalink();?>#virtue_comments"><i class="icon-bubbles"></i> <?php comments_number( '0', '1', '%' ); ?></a>
        </span>
    <?php } else { 

    // or show specific meta for desserts 
        $id = $post->ID;
        $post_desserts = get_the_terms($id, 'dessert-types'); 
        $post_dietary = get_the_terms($id, 'dietary'); 

    
        if ( $post_desserts==true ) { ?>
            <span class="postedintop"><i class="icon-drawer"></i> 
                Dessert Type: <?php the_terms($id, 'dessert-types', '', ', ') ?> 
            </span><br/>
        <?php };
        //if ($post_desserts == true && $post_dietary ==  true ) { echo "<br/>";}

        if ( $post_dietary==true ) { ?>
            <span class="postedintop"><i class="icon-drawer"></i> 
            Dietary Type: <?php the_terms($id, 'dietary', '', ', ') ?>
            </span><br/>
        <?php }; ?>
            

    <?php }; ?>

</div>




    